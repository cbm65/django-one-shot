from django.urls import path

from todos.views import todo_list_list, show_todo_list, create_list, edit_list, delete_list, create_item, edit_item


urlpatterns = [
    path("", todo_list_list, name = "todo_list_list"),
    path("<int:id>/", show_todo_list, name="show_todo_list"),
    path("create/", create_list, name="create_list"),
    path("<int:id>/edit/", edit_list, name="edit_list"),
    path("<int:id>/delete/", delete_list, name="delete_list"),
    path("items/create/", create_item, name="create_item"),
    path("items/<int:id>/edit/", edit_item, name="edit_item"),

]
