from django.shortcuts import render, get_object_or_404, redirect

from django.contrib.auth.decorators import login_required

from todos.models import TodoList, TodoItem

from todos.forms import TodoForm, ItemForm

# from todos.forms import RecipeForm
# Create your views here.

def todo_list_list(request):
  todo_lists = TodoList.objects.all()
  context = {
    "todo_lists": todo_lists,
  }
  return render(request, "todos/list.html", context)


def show_todo_list(request, id):
    #do something with the model; retrieve the data of a single instance or collection
    todo_list = get_object_or_404(TodoList, id=id)

    #add model data to context
    context = {
        "todo_list": todo_list,
    }
    #call render function passing in the request, a template and the context
    return render(request, "todos/detail.html", context)


def create_list(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            todo_list = form.save(False)

            todo_list.save()
            list = form.save()
            return redirect("show_todo_list", id=list.id)

    else:
        form = TodoForm()

    context = {
        "form": form,
    }

    return render(request, "todos/create.html", context)


def edit_list(request, id):

    todo_list = get_object_or_404(TodoList, id=id)

    if request.method == "POST":

        form = TodoForm(request.POST, instance=todo_list)
        if form.is_valid():
            form.save()
            return redirect("show_todo_list", id=id)

    else:
        form = TodoForm(instance=todo_list)

    context = {
        "todo_list": todo_list,
        "form": form,
    }


    return render(request, "todos/edit.html", context)



def delete_list(request, id):
  model_instance = TodoList.objects.get(id=id)
  if request.method == "POST":
    model_instance.delete()
    return redirect("todo_list_list")

  return render(request, "todos/delete.html")




def create_item(request):
    if request.method == "POST":
        form = ItemForm(request.POST)
        if form.is_valid():
            todo_item = form.save(False)

            todo_item.save()

            item = form.save()

            return redirect("show_todo_list", id=item.list.id)

    else:
        form = ItemForm()

    context = {
        "form": form,
    }

    return render(request, "todos/create_item.html", context)

def edit_item(request, id):

    todo_item = get_object_or_404(TodoItem, id=id)

    if request.method == "POST":

        form = ItemForm(request.POST, instance=todo_item)

        if form.is_valid():
            form.save()

            return redirect("show_todo_list", id=id)

    else:
        form = ItemForm(instance=todo_item)

    context = {
        "todo_item": todo_item,
        "form": form,
    }


    return render(request, "todos/edit_item.html", context)
